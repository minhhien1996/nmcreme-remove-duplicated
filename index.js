const fs = require('fs');
fs.writeFileSync('removed.txt', '');

const mongoose = require('mongoose');
mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost/nmcreme');
const dataSchema = mongoose.Schema({
  name: {type:String, required:'{PATH} is required!'},
  address: {type:String, required:'{PATH} is required!'},
  email: {type:String, required:'{PATH} is required!'},
  phone: {type:String, required:'{PATH} is required!'},
  url: {type:String, required:'{PATH} is required!'},
  userID: {type:String, default: null/* required:'{PATH} is required!'*/},
  nationalId: {type:String,/* required:'{PATH} is required!'*/},
  imageId: {type:String, required:'{PATH} is required!'},
  time: {type:String, required:'{PATH} is required!'}
});

const Data = mongoose.model('Data', dataSchema);

const statDuplicate = () => {
  return Promise.resolve(
    Data.find({userID: { $nin: [null, 'null'] }})
    .exec()
    .then(users => console.log('ALL USERID ', users.length))
  );
}

const statDistinct = () => {
  return Promise.resolve(
    Data.find({userID: { $nin: [null, 'null'] }})
    .distinct('userID')
    .exec()
    .then(users => console.log('DISTINCT USERID', users.length))
  );
}

Promise.resolve(console.log('PRE-WORK STATS...'))
.then(() => statDuplicate())
.then(() => statDistinct())
.then(() => {
  return Data.find({userID: { $nin: [null, 'null'] }})
  .distinct('userID')
  .exec()
  .then((distinctUserID) => {
    return Promise.all(distinctUserID.map(userID => Data.findOne({ userID }).exec()));
  })
  .then(distinctUser => {
    return Promise.all(distinctUser.map(user => Data.find({ userID: user.userID, url: { $ne: user.url } }).exec()))
  })
  .then(duplicateUser => {
    return Promise.all(
      duplicateUser.filter(eleArr => eleArr.length !== 0)
      .reduce(
        (promiseArr, eleArr) => (promiseArr.concat(
          eleArr.map(user => { fs.appendFileSync('removed.txt', user + '\n'); return Data.remove(user); })
        )), []
      )
    );
  })
  .catch(err => console.log(err));
})
.then(() => Promise.resolve(console.log('POST-WORK STATS...')))
.then(() => statDuplicate())
.then(() => statDistinct())
.then(() => {
  console.log('FINISHED');
  process.exit();
});


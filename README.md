
# nmcreme-remove-duplicated
Remove duplicated data. Duplicated data are records with the same userID, one of them will be valid, the others will be removed from database and logged to *removed.txt*

# How to use:
>>cd nmcreme-remove-duplicated <br />
>>npm install <br />
>>node index.js <br />
<br />

Removed data will be logged to *removed.txt* in nmcreme-remove-duplicated folder.<br />
